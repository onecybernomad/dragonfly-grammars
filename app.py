from tkinter import *
from kaldi_dfly_loader_plus_gui import *

BG_COLOR = "#17202A"
TEXT_COLOR = "#EAECEE"
FONT = "Helvetica 14"
FONT_BOLD = "Helvetica 13 bold"

class GuiApp:
    def __init__(self):
        self.window = Tk()
        self._setup_main_window()

    def run(self):
        self.window.mainloop()

    def _setup_main_window(self):
        self.window.title("Speech coding")
        self.window.resizable(width=False, height=False)
        self.window.configure(width=470,height=200, bg=BG_COLOR)

        head_label = Label(self.window, bg=BG_COLOR, fg=TEXT_COLOR, text="Pause, Stop, and Start speech code", font=FONT_BOLD, pady=10)
        head_label.place(relwidth=1)


if __name__ == "__main__":
    app = GuiApp()
    app.run()
