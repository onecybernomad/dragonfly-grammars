"""
Author:Jeremy Hayes
Modified from: Brandon Lovrien version

Command module for programming variable naming conventions
==========================================================
"""

from dragonfly import *

# helper function handling "camelBack"
def camel_back(command):
    someString = str(command)
    lowerString = someString.lower()
    words = lowerString.split()
    finalString = ""
    isFirst = 1
    for word in words:
        if isFirst == 1:
            finalString = finalString + word
            isFirst = 0
        else:
            finalString = finalString + word.title()
    printer = Text(finalString)
    printer.execute()

# Voice command rule for "Camel" naming convention.
def camel_format(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        upperString = someString.title()
        printer = Text(upperString.replace(' ', ''))
        printer.execute()

# Voice command rule for first caracter of every word naming convention.
def caps_first_format(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        upperString = someString.title()
        printer = Text(upperString)
        printer.execute()

# Voice command rule for first caracter of the first word.
def dictate_format(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        upperString = someString.capitalize()
        printer = Text(upperString)
        printer.execute()

# Voice command rule for "middle_underscores" naming convention.
def middle_underscores(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        printer = Text(someString.replace(' ', '_'))
        printer.execute()

# Voice command rule for "dot.notation" naming convention.
def dot_notation(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        printer = Text(someString.replace(' ', '.'))
        printer.execute()


# Voice command rule for "_BEGINNING_UNDERSCORES" naming convention.
def _BEGINNING_UNDERSCORES(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        upperString = "_" + someString.upper()
        printer = Text(upperString.replace(' ', '_'))
        printer.execute()

# Voice command rule for "_BEGINNING_UNDERSCORES" naming convention.
def _BEGINNING_dot_note(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        upperString = "." + someString.upper()
        printer = Text(upperString.replace(' ', '.'))
        printer.execute()

# Voice command rule for "middle-slash" naming convention.
def middle_lines_format(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        printer = Text(someString.replace(' ', '-'))
        printer.execute()

# Voice command rule for "middle-slash" naming convention.
def middle_slash_format(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        printer = Text(someString.replace(' ', '/'))
        printer.execute()


# Voice command rule for "spacefree" naming convention.
def SpaceFreeFormat(command):   # Callback when command is spoken.
        textToPrint = command
        someString = str(textToPrint)
        printer = Text(someString.replace(' ', ''))
        printer.execute()

class ProgrammingNamingConventions(MappingRule):

    mapping  = {

                  #both of these commands do the same thing in terms of name formatting   example: testValue
                  "variable <command>":                  Function(camel_back),
                  "variable <command> <symbol>":             Function(camel_back) + Text("%(symbol)s"),
                  "<symbol> variable <command>":              Text("%(symbol)s") + Function(camel_back),

                  "camelot <command>":                  Function(camel_back),
                  "camelot <command> <symbol>":             Function(camel_back) + Text("%(symbol)s"),
                  "<symbol> camelot <command>":              Text("%(symbol)s") + Function(camel_back),

                  #this command capitalizes the 1st letter of each word  example: Test Value
                  "caps first <command>":                      Function(caps_first_format),
                  "caps first <command> <symbol>":             Function(caps_first_format) + Text("%(symbol)s"),
                  "<symbol> caps first <command>":              Text("%(symbol)s") + Function(caps_first_format),

                  #this command capitalizes the 1st letter of first word  example: Test value
                  "dictate <command>":                      Function(dictate_format),
                  "dictate <command> <symbol>":             Function(dictate_format) + Text("%(symbol)s"),
                  "<symbol> dictate <command>":              Text("%(symbol)s") + Function(dictate_format),

                  #this command capitalizes the 1st letter of each word and removes spaces   example: TestValue
                  "camel <command>":                      Function(camel_format),
                  "camel <command> <symbol>":             Function(camel_format) + Text("%(symbol)s"),
                  "<symbol> camel <command>":              Text("%(symbol)s") + Function(camel_format),

                  #this command replaces spaces between words with underscores  example:test_value
                  "middle under <command>":         Function(middle_underscores),
                  "middle under <command> <symbol>":             Function(middle_underscores) + Text("%(symbol)s"),
                  "<symbol> middle under <command>":              Text("%(symbol)s") + Function(middle_underscores),

                  #this command replaces spaces between words with dots  example:test.value
                  "dot note <command>":         Function(dot_notation),
                  "dot note <command> <symbol>":             Function(dot_notation) + Text("%(symbol)s"),
                  "<symbol> dot note <command>":              Text("%(symbol)s") + Function(dot_notation),

                  #example of this command: _TEST_VALUE
                  "beginning under <command>":       Function(_BEGINNING_UNDERSCORES),
                  "beginning under <command> <symbol>":             Function(_BEGINNING_UNDERSCORES) + Text("%(symbol)s"),
                  "<symbol> beginning under <command>":              Text("%(symbol)s") + Function(_BEGINNING_UNDERSCORES),

                  #example of this command: _TEST_VALUE
                  "beginning dot note <command>":       Function(_BEGINNING_dot_note),
                  "beginning dot note <command> <symbol>":             Function(_BEGINNING_dot_note) + Text("%(symbol)s"),
                  "<symbol> beginning under <command>":              Text("%(symbol)s") + Function(_BEGINNING_dot_note),

                  #example of this command: test-value
                  "middle lines <command>":               Function(middle_lines_format),
                  "middle lines <command> <symbol>":             Function(middle_lines_format) + Text("%(symbol)s"),
                  "<symbol> middle lines <command>":              Text("%(symbol)s") + Function(middle_lines_format),

                  #example of this command: test-value
                  "middle slash <command>":               Function(middle_slash_format),
                  "middle slash <command> <symbol>":             Function(middle_slash_format) + Text("%(symbol)s"),
                  "<symbol> middle slice <command>":              Text("%(symbol)s") + Function(middle_slash_format),


                    # example of this command: testvalue
                  "space free <command>":                 Function(SpaceFreeFormat),
                  "space free <command> <symbol>":             Function(SpaceFreeFormat) + Text("%(symbol)s"),
                  "<symbol> space free <command>":              Text("%(symbol)s") + Function(SpaceFreeFormat),

                  # symbols
                  "dash": Text("-"),
                  "slash": Text("/"),
                  "back slash": Text("\\"),
                  "pipe": Text("|"),
                  "or": Text("||"),
                  "question": Text("?"),

                  # Numbers
                  "zero":                   Text("0"),
                  "one":                    Text("1"),
                  "(two|to)":               Text("2"),
                  "three":                  Text("3"),
                  "four":                   Text("4"),
                  "five":                   Text("5"),
                  "six":                    Text("6"),
                  "seven":                  Text("7"),
                  "eight":                  Text("8"),
                  "nine":                   Text("9"),
                  "pipe":                   Text("|"),

                  # Alpha codes
                  "alpha|arch":             Text("a"),
                  "bravo":                  Text("b"),
                  "charlie":                Text("c"),
                  "delta":                  Text("d"),
                  "echo":                   Text("e"),
                  "foxtrot":                Text("f"),
                  "golf":                   Text("g"),
                  "hotel":                  Text("h"),
                  "(india|indigo) ":        Text("i"),
                  "juliet":                 Text("j"),
                  "kilo":                   Text("k"),
                  "lima":                   Text("l"),
                  "mike":                   Text("m"),
                  "november|nora":          Text("n"),
                  "oscar":                  Text("o"),
                  "(P|papa|poppa) ":        Text("p"),
                  "(Q|quebec|quiche) ":     Text("q"),
                  "romeo":                  Text("r"),
                  "sierra":                 Text("s"),
                  "tango":                  Text("t"),
                  "uniform|unix":           Text("u"),
                  "victor":                 Text("v"),
                  "whiskey":                Text("w"),
                  "(X|xray) ":             Text("x"),
                  "yankee":                 Text("y"),
                  "zulu":                   Text("z"),

                  # Caps Alpha codes
                  "caps alpha|arch":             Text("A"),
                  "caps bravo":                  Text("B"),
                  "caps charlie":                Text("C"),
                  "caps delta":                  Text("D"),
                  "caps echo":                   Text("E"),
                  "caps foxtrot":                Text("F"),
                  "caps golf":                   Text("G"),
                  "caps hotel":                  Text("H"),
                  "caps (india|indigo) ":        Text("I"),
                  "caps juliet":                 Text("J"),
                  "caps kilo":                   Text("K"),
                  "caps lima":                   Text("L"),
                  "caps mike":                   Text("M"),
                  "caps november|nora":          Text("N"),
                  "caps oscar":                  Text("O"),
                  "caps (P|papa|poppa) ":        Text("P"),
                  "caps (Q|quebec|quiche) ":     Text("Q"),
                  "caps romeo":                  Text("R"),
                  "caps sierra":                 Text("S"),
                  "caps tango":                  Text("T"),
                  "caps uniform|unix":           Text("U"),
                  "caps victor":                 Text("V"),
                  "caps whiskey":                Text("W"),
                  "caps (X|xray) ":             Text("X"),
                  "caps yankee":                 Text("Y"),
                  "caps zulu":                   Text("Z"),
               }

    extras   = [
                Dictation("command"),
                Choice("symbol",{
                                "dot":".",
                                 "arrow":"->",
                                 "parameters":"()",
                                "parameters dot":"()."
                                }
                       )
               ]



# Create a grammar which contains and loads the command rule.
naminggrammar = Grammar("naming conventions")                # Create a grammar to contain the command rule.
naminggrammar.add_rule(ProgrammingNamingConventions())
naminggrammar.load()
