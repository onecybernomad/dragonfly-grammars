# Author: Jeremy Hayes
# Modified from: Brandon Lovrien version
# This script includes commands used for Terminal coding

from dragonfly import (Grammar, CompoundRule, Dictation, Text, Key, AppContext, MappingRule, Choice)
import os

class TerminalEnabler(CompoundRule):
    spec = "Activate Terminal"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        TerminalBootstrap.disable()
        TerminalGrammar.enable()
        s = "Terminal grammar activated"
        print (s)
        os.system('echo "{0}" | festival --tts'.format(s))

class TerminalDisabler(CompoundRule):
    spec = "switch language"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        TerminalGrammar.disable()
        TerminalBootstrap.enable()
        s = "Terminal grammar deactivated"
        print (s)
        os.system('echo "{0}" | festival --tts'.format(s))

class TerminalTestRule(CompoundRule):
    spec = "test Terminal"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        print ("Terminal grammar tested")


class TerminalTags(MappingRule):

    mapping  = {
                   "file copy":                      Text("cp"),
                   "file list":                      Text("ls"),
                   "file move":                      Text("mv"),
                   "force remove <tagname>":    Text("rm -rf %(tagname)s"),
                   "make directory":            Text("mkdir"),
                   "nano":                      Text("nano" ),
                   "node package manager":      Text("npm" ),
                   "node":                      Text("node" ),
                   "node version manager":      Text("nvm" ),
                   "python":                    Text("python3" ),
                   "pip":                       Text("pip3" ),
                   "remove":                    Text( "rm" ),

                   # used to specify tag attributes
                   "attribute":          Text( ' attributeName=""' ) + Key( "left" ),
                   "<attribute> attribute":                Text( ' %(attribute)s=""' ) + Key( "left" ),

               }
    extras = [
        Choice("attribute", {
                               "ID": "id",
                               "class": "class",
                               "style": "style",
                               "title": "title",
                               "SRC": "src",
                               "HREF": "href",
                               "type": "type",
                               "value": "value",
                               "ng": "ng-",
                             }
                    ),
        Choice("tagname", {
                              "anchor": "a",
                             }
                    )
             ]

#  Code for initial setup of the Terminal grammar
TerminalBootstrap = Grammar("Terminal bootstrap")                # Create a grammar to contain the command rule.
TerminalBootstrap.add_rule(TerminalEnabler())
TerminalBootstrap.load()


TerminalGrammar = Grammar("Terminal grammar")
TerminalGrammar.add_rule(TerminalTestRule())
TerminalGrammar.add_rule(TerminalDisabler())
TerminalGrammar.add_rule(TerminalTags())
TerminalGrammar.load()
TerminalGrammar.disable()

# Unload function which will be called by natlink at unload time.
def unload():
    global TerminalGrammar
    if TerminalGrammar: TerminalGrammar.unload()
    TerminalGrammar = None
